<%-- 
    Document   : add
    Created on : Apr 22, 2020, 3:27:09 PM
    Author     : Dalibor
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .save {
                background-color: cyan;
                color: white;
                border-radius: 10px;
                padding: 10px 20px;
                border: none;
                cursor: pointer;
            }

            h4 {
                margin-top: 50px !important;
            }
        </style>
    </head>
    <body>
        <%@include file="../template/navbar.jsp" %>
        <div class="row center wrapper">
            <div class="row">
                <p>${message}</p>
                <c:choose>
                    <c:when test="${requestScope.departmentToEdit == null}">
                        <h4>Add new department</h4>
                        <form class="col s4 push-s4" action="/njtweb/app/department/save" autocomplete="off" method="post">
                    </c:when>
                    <c:otherwise>
                        <h4>Edit department</h4>
                        <form class="col s4 push-s4" action="/njtweb/app/department/update" autocomplete="off" method="post">
                        <input id="id" type="text" name="id" class="validate" value="${requestScope.departmentToEdit.id}" hidden="true">
                    </c:otherwise>
                </c:choose>
                    <div class="row">
                        <div class="input-field">
                            <input id="shortname" type="text" name="shortname" class="validate" value="${requestScope.departmentToEdit.shortname}">
                            <label for="shortname">Shortname</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field">
                            <input id="name" type="text" name="name" class="validate" value="${requestScope.departmentToEdit.name}">
                            <label for="name">Name</label>
                        </div>
                    </div>

                        <button type="submit" id="save" class="save">Save</button>

                    </form>
            </div>
        </div>
    </body>
</html>
