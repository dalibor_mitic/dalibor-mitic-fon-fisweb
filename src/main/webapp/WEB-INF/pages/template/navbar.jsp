<%-- 
    Document   : navbar
    Created on : Apr 22, 2020, 3:21:30 PM
    Author     : Dalibor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <style>
            body {
                margin: 0; 
                padding: 0;
            }

            .navbar {
                list-style-type: none; 
                margin: 0; 
                padding: 0; 
                background-color: lightskyblue;
            }

            .navbar li {
                display: inline; 
                padding: 10px; 
                color: white; 
                line-height: 3;
            }

            .navbar a {
                text-decoration: none;
                color: white;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <nav>
            <div class="navbar nav-wrapper">
                <ul id="nav-mobile" class="left hide-on-med-and-down">
                    <li>Logged as : ${sessionScope.loginUser.firstName}</li>
                    <li><a href="/njtweb/app/department/all">All departments</a></li>
                    <li><a href="/njtweb/app/department/add">Add new department</a></li>
                    <li><a href="/njtweb/app/logout">Logout</a></li>
                </ul>
            </div>
        </nav>
    </body>

</html>
