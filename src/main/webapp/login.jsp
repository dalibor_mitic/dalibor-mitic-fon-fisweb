<%-- 
    Document   : login
    Created on : Apr 22, 2020, 3:24:12 PM
    Author     : Dalibor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <style>
            body {
                color: white;
            }

            .login {
                background-color: cyan;
                color: white;
                border-radius: 10px;
                padding: 10px 20px;
                border: none;
                cursor: pointer;
            }

            h3 {
                margin-top: 50px;
                margin-bottom: 30px;
            }      

            .input-field label,label.active {
                color: white !important;
            }

            p {
                font-weight: bold;
            }

            #email, #password {
                border-bottom: 1px solid white;
            }

        </style>
    </head>
    <body class="blue lighten-1">
        <div class="row center wrapper">
            <div class="row">
                <h3>Please login to continue!</h3>
                <p>${message}</p>
                <form class="col s4 push-s4" action="/njtweb/app/login" autocomplete="off" method="post">
                    <div class="row">
                        <div class="input-field">
                            <input id="email" type="email" name="email" class="validate white-text focus">
                            <label for="username">Email</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field">
                            <input id="password" type="password" name="password" class="validate white-text">
                            <label for="password">Password</label>
                        </div>
                    </div>

                    <button type="submit" id="Login" class="login">Log in</button>
                </form>
            </div>
        </div>
    </body>
</html>
