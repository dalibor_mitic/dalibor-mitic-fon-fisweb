/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitic.dalibor.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import mitic.dalibor.fon.fisweb.action.AbstractAction;
import mitic.dalibor.fon.fisweb.constants.PageConstants;
import mitic.dalibor.fon.fisweb.model.User;

/**
 *
 * @author Dalibor
 */
public class LoginAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession(true).getAttribute("loginUser") != null) {
            return PageConstants.VIEW_HOME;
        }

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = findUser(request, email, password);
        if (user == null) {
            request.setAttribute("message", "User does not exist!");
            return PageConstants.VIEW_LOGIN;
        } else {
            request.getSession(true).setAttribute("loginUser", user);
            return PageConstants.VIEW_HOME;
        }
    }

    private User findUser(HttpServletRequest request, String email, String password) {
        List<User> users = (List<User>) request.getServletContext().getAttribute("users");
        for (User user : users) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

}
