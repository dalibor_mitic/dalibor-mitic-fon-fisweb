/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitic.dalibor.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import mitic.dalibor.fon.fisweb.action.AbstractAction;
import mitic.dalibor.fon.fisweb.constants.PageConstants;
import mitic.dalibor.fon.fisweb.model.Department;

/**
 *
 * @author Dalibor
 */
public class EditDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        String shortname = request.getParameter("shortname");
        String name = request.getParameter("name");
        int id = Integer.parseInt(request.getParameter("id"));
        
        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        for (Department department : departments) {
            if(department.getId() == id){
                department.setName(name);
                department.setShortname(shortname);
                break;
            }
        }
        return PageConstants.VIEW_ALL_DEPARTMENTS;
    }
    
}
