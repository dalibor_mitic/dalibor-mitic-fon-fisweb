/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitic.dalibor.fon.fisweb.action.impl;

import javax.servlet.http.HttpServletRequest;
import mitic.dalibor.fon.fisweb.action.AbstractAction;
import mitic.dalibor.fon.fisweb.constants.PageConstants;

/**
 *
 * @author Dalibor
 */
public class AddDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        if(request.getSession().getAttribute("loginUser") == null){
            request.setAttribute("message", "You are not logged in!");
            return PageConstants.VIEW_LOGIN;
        }
        return PageConstants.VIEW_ADD_DEPARTMENT;
    }
    
}
