/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitic.dalibor.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import mitic.dalibor.fon.fisweb.action.AbstractAction;
import mitic.dalibor.fon.fisweb.constants.PageConstants;
import mitic.dalibor.fon.fisweb.model.Department;

/**
 *
 * @author Dalibor
 */
public class DeleteDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        Department departmentToDelete = null;
        for (Department department : departments) {
            if(department.getId() == id){
                departmentToDelete = department;
            }
        }
        departments.remove(departmentToDelete);
        return PageConstants.VIEW_ALL_DEPARTMENTS;
    }

}
