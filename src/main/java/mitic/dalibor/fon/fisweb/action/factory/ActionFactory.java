/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitic.dalibor.fon.fisweb.action.factory;

import mitic.dalibor.fon.fisweb.action.AbstractAction;
import mitic.dalibor.fon.fisweb.action.impl.AddDepartmentAction;
import mitic.dalibor.fon.fisweb.action.impl.AllDepartmentsAction;
import mitic.dalibor.fon.fisweb.action.impl.DeleteDepartmentAction;
import mitic.dalibor.fon.fisweb.action.impl.EditDepartmentAction;
import mitic.dalibor.fon.fisweb.action.impl.SetDepartmentForEditAction;
import mitic.dalibor.fon.fisweb.action.impl.LoginAction;
import mitic.dalibor.fon.fisweb.action.impl.LogoutAction;
import mitic.dalibor.fon.fisweb.action.impl.SaveDepartmentAction;
import mitic.dalibor.fon.fisweb.constants.ActionConstants;

/**
 *
 * @author Dalibor
 */
public class ActionFactory {

    public static AbstractAction createActionFactory(String actionName) {
        AbstractAction action = null;
        if (actionName.equals(ActionConstants.URL_LOGIN)) {
            action = new LoginAction();
        }
        if (actionName.equals(ActionConstants.URL_ALL_DEPARTMENTS)) {
            action = new AllDepartmentsAction();
        }
        if (actionName.equals(ActionConstants.URL_ADD_DEPARTMENTS)) {
            action = new AddDepartmentAction();
        }
        if (actionName.equals(ActionConstants.URL_LOGOUT)) {
            action = new LogoutAction();
        }
        if(actionName.equals(ActionConstants.URL_SAVE_DEPARTMENT)){
            action = new SaveDepartmentAction();
        }
        if(actionName.equals(ActionConstants.URL_SET_DEPARTMENT_FOR_EDIT)){
            action = new SetDepartmentForEditAction();
        }
        if(actionName.equals(ActionConstants.URL_DELETE_DEPARTMENT)){
            action = new DeleteDepartmentAction();
        }
        if(actionName.equals(ActionConstants.URL_EDIT_DEPARTMENT)){
            action = new EditDepartmentAction();
        }
        return action;
    }
}
