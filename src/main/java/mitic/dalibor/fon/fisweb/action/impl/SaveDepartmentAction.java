/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitic.dalibor.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import mitic.dalibor.fon.fisweb.action.AbstractAction;
import mitic.dalibor.fon.fisweb.constants.PageConstants;
import mitic.dalibor.fon.fisweb.model.Department;

/**
 *
 * @author Dalibor
 */
public class SaveDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        String shortname = request.getParameter("shortname");
        String name = request.getParameter("name");
        Department department = new Department();
        department.setName(name);
        department.setShortname(shortname);
        
        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        int lastId;
        if(!departments.isEmpty()) {
            lastId = departments.get(departments.size()-1).getId();
        } else {
            lastId = 0;
        }
        department.setId(lastId + 1);
        
        if (departments.contains(department)){
            request.setAttribute("message", "Department already exists!");
        }else{
            departments.add(department);
            request.setAttribute("message", "Department is saved!");
        }
        return PageConstants.VIEW_ADD_DEPARTMENT;
    }

}
