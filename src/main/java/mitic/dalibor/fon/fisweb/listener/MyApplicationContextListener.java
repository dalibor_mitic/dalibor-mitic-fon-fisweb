/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mitic.dalibor.fon.fisweb.listener;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import mitic.dalibor.fon.fisweb.model.Department;
import mitic.dalibor.fon.fisweb.model.User;

/**
 *
 * @author Dalibor
 */
@WebListener
public class MyApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("users", createUsers());
        sce.getServletContext().setAttribute("departments", createDepartments());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    private List<User> createUsers() {
        return new ArrayList<User>() {
            {
                add(new User("pera", "peric", "pera@pera.pera", "pera"));
                add(new User("sofija", "sofic", "sofija@sofija.sofija", "sofija"));
                add(new User("jovan", "jovanic", "jovan@jovan.jovan", "jovan"));
            }
        };
    }

    private List<Department> createDepartments() {
        return new ArrayList<Department>() {
            {
                add(new Department(1, "dep1", "Department 1"));
                add(new Department(2, "dep2", "Department 2"));
                add(new Department(3, "dep3", "Department 3"));
            }
        };
    }

}
