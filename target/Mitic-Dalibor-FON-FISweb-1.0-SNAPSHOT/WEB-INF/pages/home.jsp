<%-- 
    Document   : home
    Created on : Apr 22, 2020, 4:16:13 PM
    Author     : Dalibor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home page</title>
    </head>
    <body>
        <%@include file="template/navbar.jsp" %>
        <p>This is home page</p>
    </body>
</html>
