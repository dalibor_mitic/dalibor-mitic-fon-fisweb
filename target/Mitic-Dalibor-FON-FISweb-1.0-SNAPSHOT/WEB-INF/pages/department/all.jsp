<%-- 
    Document   : all
    Created on : Apr 22, 2020, 5:38:44 PM
    Author     : Dalibor
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>All departments</title>
        <style>
            table {
                width: 70% !important;
                margin: 50px auto;
            }
        </style>
    </head>
    <body>
        <%@include file="../template/navbar.jsp" %>
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Shortname</th>
                    <th>Name</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="department" items="${applicationScope.departments}">
                    <tr>
                        <td>${department.id}</td>
                        <td>${department.shortname}</td>
                        <td>${department.name}</td>
                        <td>
                            <a href="delete?id=${department.id}">Delete</a>
                        </td>
                        <td>
                             <a href="edit?id=${department.id}">Edit</a>
                        </td>
                    </tr>
                </c:forEach>

            </tbody>
        </table>
    </body>
</html>
